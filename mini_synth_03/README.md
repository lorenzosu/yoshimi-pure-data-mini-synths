Lo-Fi Ambient-like music with Pure Data driving Yoshimi synth on Linux and
heavily re-processing Yoshimi's audio output to produce the 'lo-fi' and add
some effects

Requirements:
- Pure Data:
- Yoshimi:
- JACK

To run:
- Start Yoshimi and open the state file mini_synth_03.state
- Start Pd and open the patch LO_FI.pd
- Connect via alsa midi Pd to Yoshimi (check Yoshimi configuration so that it
  is using alsa midi)
- Disconnect Yoshimi audio output to the system one (if it was), and instead
connected to Pd's audio input
- Connect Pd's audio output to system output
- In Pd do Media > DSP On (or CTRL+/)
- Click on the yeloow bang labelled with "START"

The patch can run indefinitely until you bang the 'FINISH': in this case it will
start a slow (ca. 45 seconds) fade out before it stops. The timings for some
intruments are thought for a duration of about 20 to 25 mins. but you can really
have it last as long as you like :-)

'lo-fi' is mostly done by:
- Recording Yoshimi's output to a buffer (2 actually for stereo), and then read
this really slowly with a phasor. This already degrades the audio a bit (and
lowers the original pitch)
- Adding a bit of noise
- DIY quantizing the output at 10 bit (plus some filtering)

Youtube demo: https://youtu.be/h6kiqR5tg80
