Project to collect mini synths using Pure Data and Yoshimi creating drone-like, automated, non-linear sequences.

See specific sub-directories for each synth project.
