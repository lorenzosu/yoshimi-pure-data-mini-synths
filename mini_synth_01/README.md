Drone / Ambient-like music with Pure Data driving Yoshimi synth on Linux.

Requirements:
- Pure Data:
- Yoshimi:
- JACK (optional but recommended)

To run:
- Start Yoshimi and open the state file mini_synth_01.state
- Start Pd and open the patch MINI_SYNTH.pd
- Connect via alsa midi Pd to Yoshimi (check Yoshimi configuration so that it
  is using alsa midi)
- In the Pure Data patch click on some of the light blue toggles (e.g. start 
  from the largest one)
- Click on the yellow bang labelled with 'Start'

Live demo on YouTube: https://youtu.be/ifdOGFZkeT0
